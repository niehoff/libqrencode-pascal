(*
  This file is converted with h2pas and additional corrected by Rudolf Niehoff
  Copyright © by Rudolf Niehoff, 2015 - http://rniehoff.de
  The Copyright of the original file "qrencode.h" has Kentaro Fukuchi - http://fukuchi.org

  This file is free software; you can redistribute it and/or modify it under the terms
  of the GNU Lesser General Public License as published by the Free Software Foundation;
  either version 3 of the License, or any later version.
*)

unit qrencode;

interface

const
  {$IFDEF WINDOWS}
    External_library='qrencode.dll';
  {$ELSE}
    External_library='libqrencode.so'; {Setup as you need}
  {$ENDIF}
type
  PChar  = ^char;

{$IFDEF FPC}
{$PACKRECORDS C}
{$ENDIF}

  QRecLevel =  Longint;

const
  QR_ECLEVEL_L = 0;
  QR_ECLEVEL_M = 1;
  QR_ECLEVEL_Q = 2;
  QR_ECLEVEL_H = 3;

type
  QRencodeMode = Longint;

const
  QR_MODE_NUL        = -1;  ///< Terminator (NUL character). Internal use only
  QR_MODE_NUM        =  0;  ///< Numeric mode
  QR_MODE_AN         =  1;  ///< Alphabet-numeric mode
  QR_MODE_8          =  2;  ///< 8-bit data mode
  QR_MODE_KANJI      =  3;  ///< Kanji (shift-jis) mode
  QR_MODE_STRUCTURE  =  4;  ///< Internal use only

    {*****************************************************************************
     * Input data (qrinput.c)
     **************************************************************************** }
    {*
     * Singly linked list to contain input strings. An instance of this class
     * contains its version and error correction level too. It is required to
     * set them by QRinput_setVersion() and QRinput_setErrorCorrectionLevel(),
     * or use QRinput_new2() to instantiate an object.
      }

    type
      //_QRinput = QRinput;
      QRinput = record
      end;
      PQRinput = ^QRinput;

    {*
     * Instantiate an input data object. The version is set to 0 (auto-select)
     * and the error correction level is set to QR_ECLEVEL_L.
     * @return an input object (initialized). On error, NULL is returned and errno
     *         is set to indicate the error.
     * @throw ENOMEM unable to allocate memory.
      }

    function QRinput_new:PQRinput;cdecl;external External_library name 'QRinput_new';

    {*
     * Instantiate an input data object.
     * @param version version number.
     * @param level Error correction level.
     * @return an input object (initialized). On error, NULL is returned and errno
     *         is set to indicate the error.
     * @throw ENOMEM unable to allocate memory for input objects.
     * @throw EINVAL invalid arguments.
      }
    function QRinput_new2(version:longint; level:QRecLevel):PQRinput;cdecl;external External_library name 'QRinput_new2';

    {*
     * Append data to an input object.
     * The data is copied and appended to the input object.
     * @param input input object.
     * @param mode encoding mode.
     * @param size size of data (byte).
     * @param data a pointer to the memory area of the input data.
     * @retval 0 success.
     * @retval -1 an error occurred and errno is set to indeicate the error.
     *            See Execptions for the details.
     * @throw ENOMEM unable to allocate memory.
     * @throw EINVAL input data is invalid.
     *
      }
(* Const before type ignored *)
    function QRinput_append(var input:QRinput; mode:QRencodeMode; size:longint; var data:byte):longint;cdecl;external External_library name 'QRinput_append';

    {*
     * Get current version.
     * @param input input object.
     * @return current version.
      }
    function QRinput_getVersion(var input:QRinput):longint;cdecl;external External_library name 'QRinput_getVersion';

    {*
     * Set version of the QR-code that is to be encoded.
     * @param input input object.
     * @param version version number (0 = auto)
     * @retval 0 success.
     * @retval -1 invalid argument.
      }
    function QRinput_setVersion(var input:QRinput; version:longint):longint;cdecl;external External_library name 'QRinput_setVersion';

    {*
     * Get current error correction level.
     * @param input input object.
     * @return Current error correcntion level.
      }
    function QRinput_getErrorCorrectionLevel(var input:QRinput):QRecLevel;cdecl;external External_library name 'QRinput_getErrorCorrectionLevel';

    {*
     * Set error correction level of the QR-code that is to be encoded.
     * @param input input object.
     * @param level Error correction level.
     * @retval 0 success.
     * @retval -1 invalid argument.
      }
    function QRinput_setErrorCorrectionLevel(var input:QRinput; level:QRecLevel):longint;cdecl;external External_library name 'QRinput_setErrorCorrectionLevel';

    {*
     * Free the input object.
     * All of data chunks in the input object are freed too.
     * @param input input object.
      }
    procedure QRinput_free(var input:QRinput);cdecl;external External_library name 'QRinput_free';

    {*
     * Validate the input data.
     * @param mode encoding mode.
     * @param size size of data (byte).
     * @param data a pointer to the memory area of the input data.
     * @retval 0 success.
     * @retval -1 invalid arguments.
      }
(* Const before type ignored *)
    function QRinput_check(mode:QRencodeMode; size:longint; var data:byte):longint;cdecl;external External_library name 'QRinput_check';

    {*
     * Set of QRinput for structured symbols.
      }

    type
      //_QRinput_Struct = QRinput_Struct;
      QRinput_Struct = record
      end;
      PQRinput_Struct = ^QRinput_Struct;
    {*
     * Instantiate a set of input data object.
     * @return an instance of QRinput_Struct. On error, NULL is returned and errno
     *         is set to indicate the error.
     * @throw ENOMEM unable to allocate memory.
      }

    function QRinput_Struct_new:PQRinput_Struct;cdecl;external External_library name 'QRinput_Struct_new';

    {*
     * Set parity of structured symbols.
     * @param s structured input object.
     * @param parity parity of s.
      }
    procedure QRinput_Struct_setParity(var s:QRinput_Struct; parity:byte);cdecl;external External_library name 'QRinput_Struct_setParity';

    {*
     * Append a QRinput object to the set.
     * @warning never append the same QRinput object twice or more.
     * @param s structured input object.
     * @param input an input object.
     * @retval >0 number of input objects in the structure.
     * @retval -1 an error occurred. See Exceptions for the details.
     * @throw ENOMEM unable to allocate memory.
      }
    function QRinput_Struct_appendInput(var s:QRinput_Struct; var input:QRinput):longint;cdecl;external External_library name 'QRinput_Struct_appendInput';

    {*
     * Free all of QRinput in the set.
     * @param s a structured input object.
      }
    procedure QRinput_Struct_free(var s:QRinput_Struct);cdecl;external External_library name 'QRinput_Struct_free';

    {*
     * Split a QRinput to QRinput_Struct. It calculates a parity, set it, then
     * insert structured-append headers.
     * @param input input object. Version number and error correction level must be
     *        set.
     * @return a set of input data. On error, NULL is returned, and errno is set
     *         to indicate the error. See Exceptions for the details.
     * @throw ERANGE input data is too large.
     * @throw EINVAL invalid input data.
     * @throw ENOMEM unable to allocate memory.
      }
    function QRinput_splitQRinputToStruct(var input:QRinput):PQRinput_Struct;cdecl;external External_library name 'QRinput_splitQRinputToStruct';

    {*
     * Insert structured-append headers to the input structure. It calculates
     * a parity and set it if the parity is not set yet.
     * @param s input structure
     * @retval 0 success.
     * @retval -1 an error occurred and errno is set to indeicate the error.
     *            See Execptions for the details.
     * @throw EINVAL invalid input object.
     * @throw ENOMEM unable to allocate memory.
      }
    function QRinput_Struct_insertStructuredAppendHeaders(var s:QRinput_Struct):longint;cdecl;external External_library name 'QRinput_Struct_insertStructuredAppendHeaders';

    {*****************************************************************************
     * QRcode output (qrencode.c)
     **************************************************************************** }
    {*
     * QRcode class.
     * Symbol data is represented as an array contains width*width uchars.
     * Each uchar represents a module (dot). If the less significant bit of
     * the uchar is 1, the corresponding module is black. The other bits are
     * meaningless for usual applications, but here its specification is described.
     *
     * <pre>
     * MSB 76543210 LSB
     *     |||||||`- 1=black/0=white
     *     ||||||`-- data and ecc code area
     *     |||||`--- format information
     *     ||||`---- version information
     *     |||`----- timing pattern
     *     ||`------ alignment pattern
     *     |`------- finder pattern and separator
     *     `-------- non-data modules (format, timing, etc.)
     * </pre>
      }
    {/< version of the symbol }
    {/< width of the symbol }
    {/< symbol data }

    type
      QRcode = record
          version : longint;
          width : longint;
          data : array of byte;
        end;
      PQRcode = ^QRcode;
    {*
     * Singly-linked list of QRcode. Used to represent a structured symbols.
     * A list is terminated with NULL.
      }
      //_QRcode_List = QRcode_List;
      _QRcode_List = record
          code : ^QRcode;
          next : ^QRcode_List;
        end;
       QRcode_List = _QRcode_List;
       PQRcode_List = ^QRcode_List;

    {*
     * Create a symbol from the input data.
     * @warning This function is THREAD UNSAFE.
     * @param input input data.
     * @return an instance of QRcode class. The version of the result QRcode may
     *         be larger than the designated version. On error, NULL is returned,
     *         and errno is set to indicate the error. See Exceptions for the
     *         details.
     * @throw EINVAL invalid input object.
     * @throw ENOMEM unable to allocate memory for input objects.
      }

    function QRcode_encodeInput(var input:QRinput):PQRcode;cdecl;external External_library name 'QRcode_encodeInput';

    {*
     * Create a symbol from the string. The library automatically parses the input
     * string and encodes in a QR Code symbol.
     * @warning This function is THREAD UNSAFE.
     * @param string input string. It must be NULL terminated.
     * @param version version of the symbol. If 0, the library chooses the minimum
     *                version for the given input data.
     * @param level error correction level.
     * @param hint tell the library how non-alphanumerical characters should be
     *             encoded. If QR_MODE_KANJI is given, kanji characters will be
     *             encoded as Shif-JIS characters. If QR_MODE_8 is given, all of
     *             non-alphanumerical characters will be encoded as is. If you want
     *             to embed UTF-8 string, choose this.
     * @param casesensitive case-sensitive(1) or not(0).
     * @return an instance of QRcode class. The version of the result QRcode may
     *         be larger than the designated version. On error, NULL is returned,
     *         and errno is set to indicate the error. See Exceptions for the
     *         details.
     * @throw EINVAL invalid input object.
     * @throw ENOMEM unable to allocate memory for input objects.
      }
(* Const before type ignored *)
    function QRcode_encodeString(_string:Pchar; version:longint; level:QRecLevel; hint:QRencodeMode; casesensitive:longint):PQRcode;cdecl;external External_library name 'QRcode_encodeString';

    {*
     * Same to QRcode_encodeString(), but encode whole data in 8-bit mode.
     * @warning This function is THREAD UNSAFE.
      }
(* Const before type ignored *)
    function QRcode_encodeString8bit(_string:Pchar; version:longint; level:QRecLevel):PQRcode;cdecl;external External_library name 'QRcode_encodeString8bit';

    {*
     * Free the instance of QRcode class.
     * @param qrcode an instance of QRcode class.
      }
    procedure QRcode_free(qrcode: PQRcode);cdecl;external External_library name 'QRcode_free';

    {*
     * Create structured symbols from the input data.
     * @warning This function is THREAD UNSAFE.
     * @param s
     * @return a singly-linked list of QRcode.
      }
    function QRcode_encodeInputStructured(var s:QRinput_Struct):PQRcode_List;cdecl;external External_library name 'QRcode_encodeInputStructured';

    {*
     * Create structured symbols from the string. The library automatically parses
     * the input string and encodes in a QR Code symbol.
     * @warning This function is THREAD UNSAFE.
     * @param string input string. It should be NULL terminated.
     * @param version version of the symbol.
     * @param level error correction level.
     * @param hint tell the library how non-alphanumerical characters should be
     *             encoded. If QR_MODE_KANJI is given, kanji characters will be
     *             encoded as Shif-JIS characters. If QR_MODE_8 is given, all of
     *             non-alphanumerical characters will be encoded as is. If you want
     *             to embed UTF-8 string, choose this.
     * @param casesensitive case-sensitive(1) or not(0).
     * @return a singly-linked list of QRcode. On error, NULL is returned, and
     *         errno is set to indicate the error. See Exceptions for the details.
     * @throw EINVAL invalid input object.
     * @throw ENOMEM unable to allocate memory for input objects.
      }
(* Const before type ignored *)
    function QRcode_encodeStringStructured(_string:Pchar; version:longint; level:QRecLevel; hint:QRencodeMode; casesensitive:longint):PQRcode_List;cdecl;external External_library name 'QRcode_encodeStringStructured';

    {*
     * Same to QRcode_encodeStringStructured(), but encode whole data in 8-bit mode.
     * @warning This function is THREAD UNSAFE.
      }
(* Const before type ignored *)
    function QRcode_encodeString8bitStructured(_string:Pchar; version:longint; level:QRecLevel):PQRcode_List;cdecl;external External_library name 'QRcode_encodeString8bitStructured';

    {*
     * Return the number of symbols included in a QRcode_List.
     * @param qrlist a head entry of a QRcode_List.
     * @return number of symbols in the list.
      }
    function QRcode_List_size(var qrlist:QRcode_List):longint;cdecl;external External_library name 'QRcode_List_size';

    {*
     * Free the QRcode_List.
     * @param qrlist a head entry of a QRcode_List.
      }
    procedure QRcode_List_free(var qrlist:QRcode_List);cdecl;external External_library name 'QRcode_List_free';

(*{$if defined(__cplusplus)}
 error
}
{$endif}
{$endif}
    { __QRENCODE_H__  }     *)

implementation


end.
