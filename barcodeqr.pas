(*
  This file is an encapsulation of "qrencode.pas" written by Rudolf Niehoff
  Use this file to get an object-oriented way to use qrencode library from Kentaro Fukuchi
  Copyright © by Rudolf Niehoff, 2015 - http://rniehoff.de

  This file is free software; you can redistribute it and/or modify it under the terms
  of the GNU Lesser General Public License as published by the Free Software Foundation;
  either version 3 of the License, or any later version.
*)

unit BarcodeQR;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Graphics, qrencode;

type
  TBarcodeQR_ECCLevel = (
    eBarcodeQR_ECCLevel_Auto = 0,
    eBarcodeQR_ECCLevel_L    = 1,
    eBarcodeQR_ECCLevel_M    = 2,
    eBarcodeQR_ECCLevel_Q    = 3,
    eBarcodeQR_ECCLevel_H    = 4
  );

  { TBarcodeQR }

  TBarcodeQR = class
  private
    fText: UTF8String;
    fECCLevel: TBarcodeQR_ECCLevel;

    fPQRcode: PQRcode;
    autoECClevelResult: QRecLevel;
    fQRbitmap: TBitmap;

    function GenerateQRcodeSpaceSaving: Boolean;
    function GenerateQRcode: Boolean;
    procedure QRcode2Bitmap(scale: Integer);
    procedure DestroyQRcode;


  public
    constructor Create(AOwner: TComponent);

    procedure PaintOnCanvas(canvas: TCanvas; rect: TRect);
    procedure SaveToFile(const filename: String);
    function GetAutoECCLevelResult: TBarcodeQR_ECCLevel;
  published
    property Text: UTF8String read fText write fText;
    property ECCLevel: TBarcodeQR_ECCLevel read fECCLevel write fECCLevel default eBarcodeQR_ECCLevel_Auto;
  end;

implementation

{ TBarcodeQR }

function TBarcodeQR.GenerateQRcodeSpaceSaving: Boolean;
var fPtestQRcode: PQRcode;
    pc: PChar;
    i: Integer;
    ecl: QRecLevel;
begin try
  pc := PChar(fText);
  ecl := 0;
  autoECClevelResult := 0;

  fPtestQRcode := QRcode_encodeString(pc,0,ecl,QR_MODE_8,1);

  repeat
    ecl := ecl + 1;

    fPQRcode := fPtestQRcode;
    fPtestQRcode := QRcode_encodeString(pc,0,ecl,QR_MODE_8,1);

    if fPQRcode^.width < fPtestQRcode^.width then begin
      QRcode_free(fPtestQRcode);
      Exit(true);
    end;
    QRcode_free(fPQRcode);

    autoECClevelResult := ecl;
  until ecl = 3;

  fPQRcode := fPtestQRcode;
  Exit(true);

except
  fPQRcode := nil;
  Exit(false);
end; end;

function TBarcodeQR.GenerateQRcode: Boolean;
var ecl: QRecLevel;
begin
  autoECClevelResult := -1; //Reset der Variable falls kein Auto eingestellt wurde

  case fECCLevel of
    eBarcodeQR_ECCLevel_L: ecl := QR_ECLEVEL_L;
    eBarcodeQR_ECCLevel_M: ecl := QR_ECLEVEL_M;
    eBarcodeQR_ECCLevel_Q: ecl := QR_ECLEVEL_Q;
    eBarcodeQR_ECCLevel_H: ecl := QR_ECLEVEL_H;
  else ecl := QR_ECLEVEL_L;
  end;

  if fText = '' then Exit(false);

  if fECCLevel <> eBarcodeQR_ECCLevel_Auto then try
    fPQRcode := QRcode_encodeString(PChar(fText),0,ecl,QR_MODE_8,1);
  except
    fPQRcode := nil;
    Exit(false);
  end else Exit(GenerateQRcodeSpaceSaving);

  Exit(true);
end;

procedure TBarcodeQR.QRcode2Bitmap(scale: Integer);
var x, y, i: Integer;
begin
  fQRbitmap.SetSize(fPQRcode^.width*scale,fPQRcode^.width*scale);
  fQRbitmap.Clear;
  fQRbitmap.Canvas.Brush.Color:= clWhite;

  i := 0;
  for y := 0 to fPQRcode^.width -1 do for x := 0 to fPQRcode^.width -1 do begin

    //if ((fPQRcode^.data[i]) and 1) = 1 then else
    //  fQRbitmap.Canvas.Pixels[x,y] := clWhite;

    if ((fPQRcode^.data[i]) and 1) = 1 then else
      fQRbitmap.Canvas.FillRect(x*scale,y*scale,(x+1)*scale,(y+1)*scale);

    i := i + 1;
  end;
end;

procedure TBarcodeQR.DestroyQRcode;
begin
  if Assigned(fPQRcode) then try
    QRcode_free(fPQRcode);
  finally
    fPQRcode := nil;
  end;
end;

constructor TBarcodeQR.Create(AOwner: TComponent);
begin
  inherited Create;
end;

procedure TBarcodeQR.PaintOnCanvas(canvas: TCanvas; rect: TRect);
var scale: Integer;
    x, y, i: Integer;
begin
  if not(GenerateQRcode) then Exit;

  scale := rect.Right - rect.Left;
  if rect.Bottom - rect.Top < scale then scale := rect.Bottom - rect.Top;

  scale := scale div fPQRcode^.width;
  if scale = 0 then Exit;

  fQRbitmap := TBitmap.Create;
  QRcode2Bitmap(scale);
  canvas.Draw(rect.Left,rect.Top,fQRbitmap);
  fQRbitmap.Free;

  DestroyQRcode;
end;

procedure TBarcodeQR.SaveToFile(const filename: String);
var pic: TPicture;
begin
  if not(GenerateQRcode) then Exit;

  pic := TPicture.Create;

  fQRbitmap := pic.Bitmap;
  QRcode2Bitmap(1);

  pic.SaveToFile(filename);
  pic.Free;

  DestroyQRcode;
end;

function TBarcodeQR.GetAutoECCLevelResult: TBarcodeQR_ECCLevel;
begin
  GetAutoECCLevelResult := TBarcodeQR_ECCLevel(autoECClevelResult+1);
end;

end.

